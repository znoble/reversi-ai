#!/usr/bin/env python3

#
# PURPOSE: Count all possible board states in a game of reversi
#          with board size WIDTH x WIDTH
#      

#
# PROCESS: 'play' a game and count all possible moves each player can make
#          
#

import sys
import pickle
from random import choice, shuffle

# don't hog cpu time
import os
pid = os.getpid()
os.setpriority(os.PRIO_PROCESS, pid, 19)
print(os.getpriority(os.PRIO_PROCESS, pid))
#


WIDTH     =  6
BAD       = -2

DEBUG = False

directions = ((1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1),(1,1))

def convert_to_string(board, who):
    li = []
    for i in range(WIDTH):
        for j in range(WIDTH):
            li.append(board[(i,j)])
    return "".join(list(map(str, li))+["-", str(who)])

def getstate(board, key):

    if key not in board:
        return BAD
    return board[key]

def setstate(board, key, value):

    if key not in board:
        return False
    board[key] = value
    return True

def validmove(board, key, who, testonly=True):

    opp = 3 - who

    if getstate(board, key) != 0:
        return False

    flag = False                # have I found a good direction yet?
    for dir in directions:
        n = 0
        cell = tuple([key[i] + dir[i] for i in range(2)])
        stack = []                             
        while getstate(board, cell) == opp:    
            n += 1
            stack.append(cell)                 
            cell = tuple([cell[i] + dir[i] for i in range(2)])  
        if n > 0 and getstate(board, cell) == who:
            if not testonly:
                for cell in stack:
                    setstate(board, cell, who)
            flag = True
    if flag and not testonly:
        setstate(board, key, who)
    return flag

def getlegalmoves(board, who):

    legal = []
    for key in board:
        if validmove(board, key, who):
            legal.append(key)
    return legal

def gameover(board):

    if 0 not in board.values():
        return True
    if len(getlegalmoves(board, 1)) == 0 and len(getlegalmoves(board, 2)) == 0:
        return True
    return False
 
def printboard(board):
    for i in range(WIDTH):
        for j in range(WIDTH):
            print(board[(i,j)], end=' ')
        print()
    print()


def getnextmove(board, who, depth):
    global positions    

    movelist = getlegalmoves(board, who)

    for move in movelist:
        b = {x:board[x] for x in board}
        if validmove(b, move, who, testonly=False):
            positions.add(convert_to_string(b, who))
            if not gameover(b) and depth <= 7: 
                # if opp can't move, go again
                if len(getlegalmoves(b, 3 - who)) == 0:
                    getnextmove(b, who, depth+1)
                # if opp can move, count it.
                else:
                    getnextmove(b, 3 - who, depth+1)
            if len(positions) % 1000 == 0:
                print(len(positions))
    

if  __name__ == "__main__":

    positions = set()
    initboard = [ [],
                  [(WIDTH//2-1, WIDTH//2-1), (WIDTH//2,WIDTH//2)],
                  [(WIDTH//2-1, WIDTH//2), (WIDTH//2,WIDTH//2-1)] ]

    # set inital board
    board = {(i,j):0 for i in range(WIDTH) for j in range(WIDTH)}
    for k in board:
        setstate(board, k, 0)

    for i in range(1,3):
        for k in initboard[i]:
            setstate(board,k,i)

    # add initial board 
    positions.add(convert_to_string(board, 0))
    # recurse through all possible moves.
    getnextmove(board, 1, 1)
    print(len(positions))


    ### write db.obj and reversi.data files
    db = {}
    for p in positions:
        db[p] = [0,0,0]

    with open("reversi.data", "w") as fd:
        for p in positions:
            fd.write("".join([p, "\n"]))

    with open("db.obj", "wb") as fd:
        pickle.dump(db, fd)


#!/usr/bin/env python3
import sys
import pickle
from random import choice, shuffle 

INFO  = False
DEBUG = False 

WIDTH     =  6
BAD       = -2

directions = ((1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1),(1,1))
saved_boards = [] 

def convert_to_string(board, who):
    if who == -1:
        who = 2
    li = []
    for i in range(WIDTH):
        for j in range(WIDTH):
            li.append(board[(i,j)])
    return "".join(list(map(str, li))+["-", str(who)])


def getdb():
    with open("db.obj", "rb") as fd:
        db = pickle.load(fd)
    return db

db = getdb()

def putdb(db):
    with open("db.obj", "wb") as fd:
        pickle.dump(db, fd)

def getstate(board, key):
    if key not in board:
        return BAD
    return board[key]

def setstate(board, key, value):

    if key not in board:
        return False
    board[key] = value
    return True

def gameover(board):

    if 0 not in board.values():
        return True
    if len(getlegalmoves(board, 1)) == 0 and len(getlegalmoves(board, -1)) == 0:
        return True
    return False

def endgame(board):
    # save all the moves done in this game.
    starting_zeros = len([c for c in saved_boards[0] if c == '0'])
    whichplayer = 1 if starting_zeros == 31 else 2
    score = getscore(board)
    result = 0
    if score > 0: result = 1
    elif score < 0: result = 2

    for board in saved_boards:
        if board not in db:
            db[board] = [0,0,0]
        if result == whichplayer:
            db[board][1] += 1 # update win
        elif result == 0:
            db[board][0] += 1 # update tie
        else:
            db[board][2] += 1 # update loss
    putdb(db)

def validmove(board, key, who, testonly=True):

    opp = -who

    if getstate(board, key) != 0:
        return False

    flag = False                # have I found a good direction yet?
    for dir in directions:
        n = 0
        cell = tuple([key[i] + dir[i] for i in range(2)])
        stack = []                             # a list of cells to reverse
        while getstate(board, cell) == opp:    # move over all opponent cells
            n += 1
            stack.append(cell)                 # save the cell, in case the direction works
            cell = tuple([cell[i] + dir[i] for i in range(2)])  # continue moving
        if n > 0 and getstate(board, cell) == who:
            if testonly:
                return True
            for cell in stack:
                setstate(board, cell, who)
            flag = True
    if flag and not testonly:
        setstate(board, key, who)
    return flag

def getlegalmoves(board, who):

    legal = []
    for key in board:
        if validmove(board, key, who):
            legal.append(key)
    return legal

def getscore(board):

    count = [0,0,0]
    for v in board.values():
        count[v] += 1
    return count[1] - count[2]

def boardvalue(board):
    
    score = [0]*3
    for i in range(WIDTH):
        for j in range(WIDTH):
            k = (i,j)
            v = board[k]
            score[v] += 1
            if i in [0, WIDTH-1]:
                score[v] += 1
            if j in [0, WIDTH-1]:
                score[v] += 1
    return score

def boardeval(board, movelist, who):
    best = -1000000
    savemove = None
    for move in movelist:
        b = {x:board[x] for x in board}
        if validmove(b, move, who, testonly=False):
            scorelist = boardvalue(b)
            score = scorelist[who] - scorelist[-who]
            if score > best:
                best = score
                savemove = move
    return savemove
       
def rollout(board, who):
    value = getscore(board)
    while not gameover(board):
        movelist = getlegalmoves(board, who)
        if len(movelist) == 0:
            who = -who
            continue
        shuffle(movelist)
        move = boardeval(board, movelist, who)
        if validmove(board, move, who, testonly=False):
            value = getscore(board)
        who = -who
    return value

def montecarlo(board, who):
        
    movelist = getlegalmoves(board, who)
    if len(movelist) == 0:
        print("ERROR from montecarlo: No legal moves.", file=sys.stderr)
        return None
    
    movevalues = {}
    for move in movelist:
        b = {x:board[x] for x in board}
        if validmove(b, move, who, testonly=False):
            twl = [0,0,0]
            for i in range(5000):
                v = rollout(b, -who)
                result = 0
                if v < 0: result = 2
                elif v > 0: result = 1
                twl[result] += 1
            value = (twl[1] - twl[2]) / sum(twl)
            movevalues[move] = value
            
    getbest = max if who == 1 else min
    best = getbest(movelist, key=lambda x: movevalues[x])
    return best
            
def getmove(board, who):

    newboard = {x:board[x] for x in board}
    for k in newboard:
        if newboard[k] == 2:
            newboard[k] = -1
    
    if who == 2:
        who = -1

    move = montecarlo(newboard, who)
    _ = validmove(newboard, move, who, testonly=False)  # make the move here so we can save it to db
    for k in newboard:                                  # convert -1 -> 2 
        if newboard[k] == -1:
            newboard[k] = 2
    saved_boards.append((convert_to_string(newboard, who)))        # save the move made and the value it changed the board by

    return move        

if __name__ == "__main__":
    pass

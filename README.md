# Reversi AI

Assignment 2 for CS 575 Artificial Intelligence 

This AI will play a game of 6x6 Reversi. It uses a combination of a database populated using Monte Carlo simulations and alpha beta pruning for look ahead.

This project only contains the AI files. The files to play the game are not mine to share.

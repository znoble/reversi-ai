#!/usr/bin/env python3
import sys
import pickle
from random import shuffle

INFO  = False
DEBUG = False 
UPDATE_DB = False
WIDTH     =  6
BAD       = -2

directions = ((1,0),(1,-1),(0,-1),(-1,-1),(-1,0),(-1,1),(0,1),(1,1))
saved_boards = []

def convert_to_string(board, who):
    if who == -1:
        who = 2
    li = []
    for i in range(WIDTH):
        for j in range(WIDTH):
            c = board[(i,j)]
            if c == -1: c = 2
            li.append(c)
    return "".join(list(map(str, li))+["-", str(who)])

def getdb():
    with open("db.obj", "rb") as fd:
        db = pickle.load(fd)
    return db

db = getdb()

def putdb(db):
    with open("db.obj", "wb") as fd:
        pickle.dump(db, fd)

def printboard(board):

    for i in range(WIDTH):
        for j in range(WIDTH):
            print("{0:2}".format(board[(i,j)]), end='  ')
        print()
    print()  

def endgame(board):
    # save all the moves done in this game.
    # weight each move based on the value gained from the previous board
    starting_zeros = len([c for c in saved_boards[0] if c == '0'])
    whichplayer = 1 if starting_zeros == 31 else 2
    score = getscore(board)
    result = 0
    if score > 0: result = 1
    elif score < 0: result = 2

    for board in saved_boards:
        if board not in db:
            db[board] = [0,0,0]
        if result == whichplayer:
            db[board][1] += 1 # update win
        elif result == 0:
            db[board][0] += 1 # update tie
        else:
            db[board][2] += 1 # update loss
    putdb(db)


def getstate(board, key):
    if key not in board:
        return BAD
    return board[key]

def setstate(board, key, value):

    if key not in board:
        return False
    board[key] = value
    return True

def gameover(board):

    if 0 not in board.values():
        return True
    if len(getlegalmoves(board, 1)) == 0 and len(getlegalmoves(board, -1)) == 0:
        return True
    return False

def validmove(board, key, who, testonly=True):

    opp = -who

    if getstate(board, key) != 0:
        return False

    flag = False                
    for dir in directions:
        n = 0
        cell = tuple([key[i] + dir[i] for i in range(2)])
        stack = []                             
        while getstate(board, cell) == opp:    
            n += 1
            stack.append(cell)                 
            cell = tuple([cell[i] + dir[i] for i in range(2)])  
        if n > 0 and getstate(board, cell) == who:
            if not testonly:
                for cell in stack:
                    setstate(board, cell, who)
            flag = True
    if flag and not testonly:
        setstate(board, key, who)
    return flag

def getlegalmoves(board, who):

    legal = []
    for key in board:
        if validmove(board, key, who):
            legal.append(key)
    return legal

def getscore(board):

    count = [0,0,0]
    for v in board.values():
        count[v] += 1
    return count[1] - count[2]


def readdb(board, who):
    movelist = getlegalmoves(board, who)
    shuffle(movelist)
    best = -1000000
    savedmove = None
    savedmovescore = None
    for move in movelist:
        b = {x:board[x] for x in board}
        if validmove(b, move, who, testonly=False):
            key = convert_to_string(b, who)
            try:
                t,p1,p2 = db[key]
            except KeyError:
                return None, savedmovescore
            if (t+p1+p2) == 0:
                continue
            value = (p1 - p2) / (t+p1+p2)
            if value > best:
                best = value
                savedmove = move
                savedmovescore = getscore(b)
    return savedmove, savedmovescore 


def alphabeta(depth, board, who):
    
    movelist = getlegalmoves(board,who)   #
    shuffle(movelist)                     #
    if len(movelist) == 0:
        return None, getscore(board)      # 
    bestvalue = -1000*who
    for move in movelist:
        b = {x:board[x] for x in board}
        if validmove(b, move, who, testonly=False):    #
            value = getscore(b)                        #
            if not gameover(b) and depth < 4:          #
                _, value = alphabeta(depth+1, b, -who) #
            if value * who > bestvalue * who:
                bestmove, bestvalue = move, value
        else:
            print("ERROR: alphabeta returned an illegal move!", file=sys.stderr)
    return bestmove, bestvalue


def getmove(board, who):
    newboard = {x:board[x] for x in board}
    for k in newboard:
        if newboard[k] == 2:
            newboard[k] = -1
    
    if who == 2:
        who = -1
    
    if len(getlegalmoves(newboard, who)) == 0:
        return None

#   read move from db
    move, value = readdb(newboard, who)
#   if board not in db
#   use look ahead
    if move is None: 
        move, value = alphabeta(1, newboard, who)
    if INFO:
        print(f"AI can win by {value}")
    
    for k in newboard:
        if newboard[k] == -1:
            newboard[k] = 2

    _ = validmove(newboard, move, who, testonly=False)
    saved_boards.append(convert_to_string(newboard, who))
    return move
            

if __name__ == "__main__":
    pass
